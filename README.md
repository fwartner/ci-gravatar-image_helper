ci-gravatar-image_helper
========================

Gravatar Image-Helper for CodeIgniter

## Configuration ##

To create an "global" instance of the helper, you have to autoload the helper within the <pre>autoload.php</pre> in <pre>application/config</pre>

## Usage ##

To get an image from a gravatar-profile, you have to call the main-method of the helper:

```php
<?php echo get_gravatar($email, $size); ?>
```

Better example:
```php
<img src="<?php echo get_gravatar($email, $size); ?>" alt="My cool image" />
```